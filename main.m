% 
% Sterowanie Procesami 
% Projekt 1
% autor: Grzegorz Kamiński
% 
clc
s=tf('s')
Gs=(s+1.5)*(s+9.5)/(s-12)/(s+11)/(s+13);

% 1)transmitancja dyskretna
Gz=c2d(Gs,0.5,'zoh');
% 2)reprenentacja modelu dyskretnego
% w przestrzeni stanu
numz=Gz.num{1};
denumz=Gz.den{1};
%przejście z transmitancji dyskretnej ne model w przestrzeni stanu
[A,B,C,D]=tf2ss(numz,denumz);
%model w przestrzeni stanu metoda druga bezpośrednia
A2=A';
B2=C';
C2=B';
D2=D;
% 3)Stabilność G(s) i G(z)
% [zz,pz,kz]=tf2zpk(numz,denumz);
% step(Gs,'-',Gz,'--')


% 4)regulator sprzężony od stanu

% a)3 takie same bieguny rzeczywiste

Ka=acker(A2,B2,[0.5 0.5 0.5])
K=Ka;
sim('symul_4a');
k=1:1:length(time);

figure(1)
hold on
stairs(k,x1,'r')
stairs(k,x2,'b')
stairs(k,x3,'g')
legend('x1(k)','x2(k)','x3(k)')
xlim([0 length(time)])
xlabel('k')
ylabel('X(k)')
title('Regulator ze sprzężeniem od stanu bieguny [0,5 0,5 0,5]') 
hold off

figure(2)
stairs(k,u,'y')
xlim([0 length(time)])
xlabel('k')
ylabel('U(k)')
title('Regulator ze sprzężeniem od stanu bieguny [0,5 0,5 0,5]')
hold off

% wykresy jakości Ju i Jx
Jx=zeros(1,length(time));
sumax=zeros(1,1);
sumau=zeros(1,1);
Ju=zeros(length(time));
Jx(1)=2^2+2^2+3^2;
sumax=Jx(1);
Ju(1)=u(1)^2;
sumau=Ju(1);
for i=2:1:length(k)
    sumax=sumax+x1(i)^2 + x2(i)^2 + x3(i)^2;
    sumau=sumau+u(i)^2;
    Jx(i)= sumax/i;
    Ju(i)= sumau/i;
end

figure(3)
hold on
stairs(k,Jx,'r')
stairs(k,Ju,'b')
xlim([0 length(time)])
xlabel('k')
ylabel('Jx(k),Ju(k)')
legend('Jx(k)','Ju(k)')
title('Regulator ze sprzężeniem od stanu bieguny [0,5 0,5 0,5]')
hold off




figure
hold on
stairs(k,x1,'r')
legend('x1(k)')
xlim([0 length(time)])
xlabel('k')
ylabel('Y(k)')
title('Regulator ze sprzężeniem od stanu bieguny [0,5 0,5 0,5]') 
hold off


