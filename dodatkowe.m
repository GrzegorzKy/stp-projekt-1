% cz�� dodatkowa
% 1) regulator ze sprz od stanu
% wersja z  calkowaniem
clc
% macierze rozszerzone
Ar=[0 C2
    0 A2(1,:)
    0 A2(2,:)
    0 A2(3,:)];
Br=[0;B2];
% wektor sprz�e� zwrotnych
Kd=acker(Ar,Br,[0.5 0.5 0.5 0.5])
Tp=0.05; %czas pr�bkowania
Tc=0.5; %czas ca�kowania

% sim('symul_dodatkowe');
% kd=1:1:length(timed);

% figure(1)
% hold on
% stairs(kd,x1d,'r')
% stairs(kd,yzad,'b')
% legend('y(k)','yzad(k)')
% xlim([0 length(timed)])
% xlabel('k')
% ylabel('y(k)')
% title('Regulator z ca�kowaniem bieguny - 4x0,5') 
% hold off

figure(1)
hold on
stairs(kd,x1d,'r')

% wykresy jako�ci Ju i Jx
% Jx=zeros(1,length(timed));
% sumax=zeros(1,1);
% sumau=zeros(1,1);
% Ju=zeros(length(timed));
% Jx(1)=0;
% sumax=Jx(1);
% Ju(1)=ud(1)^2;
% sumau=Ju(1);
% for i=2:1:length(kd)
%     sumax=sumax+x1d(i)^2 + x2d(i)^2 + x3d(i)^2;
%     sumau=sumau+ud(i)^2;
%     Jx(i)= sumax/i;
%     Ju(i)= sumau/i;
% end
% 
% figure(3)
% hold on
% stairs(kd,Jx,'r')
% stairs(kd,Ju,'b')
% xlim([0 length(timed)])
% xlabel('k')
% ylabel('Jx(k),Ju(k)')
% legend('Jx(k)','Ju(k)')
% title('Regulator z ca�kowaniem bieguny - 4x0,5') 
% hold off

% 2)zak��cenie
% a- warto�� ko�cowa
% b - op�nienie [s]
% a=15;
% b=50;
% sim('symul_dodatkowe');
% kd=1:1:length(timed);
% figure
% hold on
% stairs(kd,x1d,'r')
% % stairs(kd,vd,'b')
% p1=[0,1];
% p2=[length(kd),1];
% plot([p1(1) p2(1)],[p1(2) p2(2)],'--g','LineWidth',0.001)
% legend('y(k)','vd(k)')
% xlim([0 length(timed)])
% ylim([0 a/4])
% xlabel('k')
% title(sprintf('Regulator z ca�kowaniem - zak��cenie S(%d,%d)',a,b))
% hold off

% 3)b��d modelowania
% B2 =
% 
%     0.0649
%    -0.0886
%     0.0301
Br=[0;0.08;-0.09;0.02];
Kd=acker(Ar,Br,[0.5 0.5 0.5 0.5])
sim('symul_dodatkowe');
kd=1:1:length(timed);


stairs(kd,x1d,'b')
legend('y(k)','y_u(k)')
xlim([0 length(timed)])
% ylim([0 1])
xlabel('k')
title(sprintf('Regulator z ca�kowaniem - B[%f,  %f  ,%f]',Br(3),Br(3),Br(4)))
hold off