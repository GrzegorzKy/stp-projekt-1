clc
% 4)regulator sprzężony od stanu
% uruchamiac po mainie
z1=0.75;
z2=0.05;
z3=z2;
% b)1 biegun dominujący + 2 szybkie
Kb=acker(A2,B2,[z1 z2 z3]);
K=Kb;
sim('symul_4a');
k=1:1:length(time);

figure(1)
hold on
stairs(k,x1,'r')
stairs(k,x2,'b')
stairs(k,x3,'g')
legend('x1(k)','x2(k)','x3(k)')
xlim([0 length(time)])
xlabel('k')
ylabel('X(k)')
title(sprintf('Regulator ze sprzężeniem od stanu bieguny [%f %f %f]',z1,z2,z3))
hold off

figure(2)
stairs(k,u,'y')
xlim([0 length(time)])
xlabel('k')
ylabel('U(k)')
title(sprintf('Regulator ze sprzężeniem od stanu bieguny [%f %f %f]',z1,z2,z3))
hold off

% wykresy jakości Ju i Jx
Jx=zeros(1,length(time));
sumax=zeros(1,1);
sumau=zeros(1,1);
Ju=zeros(length(time));
Jx(1)=2^2+2^2+3^2;
sumax=Jx(1);
Ju(1)=u(1)^2;
sumau=Ju(1);
for i=2:1:length(k)
    sumax=sumax+x1(i)^2 + x2(i)^2 + x3(i)^2;
    sumau=sumau+u(i)^2;
    Jx(i)= sumax/i;
    Ju(i)= sumau/i;
end

figure(3)
hold on
stairs(k,Jx,'r')
stairs(k,Ju,'b')
xlim([0 length(time)])
xlabel('k')
ylabel('Jx(k),Ju(k)')
legend('Jx(k)','Ju(k)')
title(sprintf('Regulator ze sprzężeniem od stanu bieguny [%f %f %f]',z1,z2,z3))
hold off