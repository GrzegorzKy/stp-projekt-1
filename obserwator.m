clc
% 5)pe�nego rz�du
% uruchamiac po mainie i zad4b
% a)wolny
L=acker(A2,B2,[0.9 .9 .9]);

sim('symul_5');
ko=1:1:length(timeo);
figure
hold on
stairs(ko,x_1,'-r')
stairs(ko,xo1,'--b')
legend('x1(k)','xo1(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X1(k)')
title('Obserwator pe�nego stanu wolny [0,9 0,9 0,9]') 
hold off

figure
hold on
stairs(ko,x_2,'-r')
stairs(ko,xo2,'--b')
legend('x2(k)','xo2(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X2(k)')
title('Obserwator pe�nego stanu wolny [0,9 0,9 0,9]')
hold off

figure
hold on
stairs(ko,x_3,'-r')
stairs(ko,xo3,'--b')
legend('x3(k)','xo3(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X3s(k)')
title('Obserwator pe�nego stanu wolny [0,9 0,9 0,9]')
hold off
% b)szybki
L=acker(A2,B2,[0.1 .1 .1]);

sim('symul_5');
ko=1:1:length(timeo);
figure
hold on
stairs(ko,x_1,'-r')
stairs(ko,xo1,'--b')
legend('x1(k)','xo1(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X1(k)')
title('Obserwator pe�nego stanu szybki [0.1 .1 .1]') 
hold off

figure
hold on
stairs(ko,x_2,'-r')
stairs(ko,xo2,'--b')
legend('x2(k)','xo2(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X2(k)')
title('Obserwator pe�nego stanu szybki [0.1 .1 .1]')
hold off

figure
hold on
stairs(ko,x_3,'-r')
stairs(ko,xo3,'--b')
legend('x3(k)','xo3(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X3s(k)')
title('Obserwator pe�nego stanu szybki [0.1 .1 .1]')
hold off
% c)baaardzo szybki
L=acker(A2,B2,[0.01 .01 .01]);

sim('symul_5');
ko=1:1:length(timeo);
figure
hold on
stairs(ko,x_1,'-r')
stairs(ko,xo1,'--b')
legend('x1(k)','xo1(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X1(k)')
title('Obserwator pe�nego stanu bardzo szybki [0,01 0,01 0,01]') 
hold off

figure
hold on
stairs(ko,x_2,'-r')
stairs(ko,xo2,'--b')
legend('x2(k)','xo2(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X2(k)')
title('Obserwator pe�nego stanu bardzo szybki [0,01 0,01 0,01]') 
hold off

figure
hold on
stairs(ko,x_3,'-r')
stairs(ko,xo3,'--b')
legend('x3(k)','xo3(k)')
xlim([0 length(timeo)])
xlabel('k')
ylabel('X3s(k)')
title('Obserwator pe�nego stanu bardzo szybki [0,01 0,01 0,01]') 
hold off